
/* 
 Теоретичні питання
 1. Що таке цикл в програмуванні?

 - це функція яка дозволяє виконувати блок коду кілька разів. Цикли визначають умову коли продовжувати виконання блоку коду, а коли виходити з циклу.

 2. Які види циклів є в JavaScript і які їх ключові слова?

 - for - коли відомо скільки буде циклів (ітерацій) - ключове слово for, break, continue

 - while - коли не віодомо скільки буде циклів - ключове слово while, break, continue

 -do while - ключове слово - do while, break, continue

 3. Чим відрізняється цикл do while від while?

 - основна відмінність в тому, що в do while блок коду виконується принаймні один раз, навіть якщо умова не виконується, а в while Умова перевіряється 
   перед кожною ітерацією циклу.
   Тобто у випадку з do {} while() спочатку виконається код, потім буде поставлене запитання чи хоче юзер продовжувати.
 
   */
/*
 Практичні завдання
 1. Запитайте у користувача два числа. 
 Перевірте, чи є кожне з введених значень числом. 
 Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом. 
 Виведіть на екран всі цілі числа від меншого до більшого за допомогою циклу for.
 */

 
 let num1;
 let num2;

do {
    num1 = +prompt("Enter first number:");
}

while (isNaN(num1));

do {
    num2 = +prompt(`Enter second greater number than ${num1 + 1}:`);
}
 while (isNaN(num2) || num2 <= num1 + 1);


for (let i = num1 + 1; i < num2; i++) { // використав num1 += 1 щоб в консолі відображалось наступне число від того, яке ввів користувач

    console.log(`Integer:`, i);   
}

 /*
 2. Напишіть програму, яка запитує в користувача число та перевіряє, 
 чи воно є парним числом. Якщо введене значення не є парним числом, 
 то запитуйте число доки користувач не введе правильне значення.
*/

let userNum1;

do {
    userNum1 = +prompt("Please, enter an even number:");

    if (isNaN(userNum1) || userNum1 % 2 !== 0) {
        userNum1 = prompt("Please, enter an even number:");
    }

} while (userNum1 % 2 !== 0);

console.log(`Your even number is: ${userNum1}`)
